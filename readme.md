In this project took part Olga Munka and Sergey Chernyuk.
web-site - https://sergeychernyuk.github.io/Step-Project-2-Forkio/

Task №1 - Olga Munka:
- Section Header with menu/dropmenu;
- Section "People are talking About Fork".


Task №2 - Sergey Chernyuk:
- Section "Revolutionary Editor";
- Section "Here is what you get";
- Section "Fork Subscription Pricing".

Technologies used:

HTML, CSS, Java Script, NPM, Gulp, SCSS, a lot of Gulp plugins.