const burger = document.querySelector(".menu__burger");
const menu = document.querySelector(".dropdown");

burger.addEventListener("click", function (event) {
  event.preventDefault();
  if (burger.classList.contains("active")) {
    burger.classList.remove("active");
    menu.classList.remove("active");
  } else {
    burger.classList.add("active");
    menu.classList.add("active");
  }
});

["resize", "load"].forEach(function (event) {
  window.addEventListener(
    event,
    function () {
      callBackOnMediaChange();
    },
    false
  );
});

function callBackOnMediaChange() {
  if (window.innerWidth >= 481) {
    menu.classList.remove("active");
    burger.classList.remove("active");
  }
}
